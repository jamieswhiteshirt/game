package com.trentv4.game.content.mob;

import com.trentv4.game.core.DisplayManager;

public class Entity
{
	public double x;
	public double y;
	public double xSize;
	public double ySize;
	
	public double yVelocity;
	public double xVelocity;
	public String texture;
	
	public Entity(String texture)
	{
		this.x = 0;
		this.y = 0;
		this.xSize = 0;
		this.ySize = 0;
		this.texture = texture;
	}
	
	public Entity(String texture, double x, double y, double xSize, double ySize)
	{
		this.x = x;
		this.y = y;
		this.xSize = xSize;
		this.ySize = ySize;
		this.texture = texture;
	}
	
	public void applyPhysics()
	{
		//gravity
		if(y+ySize < DisplayManager.getProperties().getHeight())
		{
			yVelocity += 0.01F;
		}
		
		//max x speed
		if(xVelocity >= 2)
		{
			xVelocity = 2;
		}
		
		//right
		if(x + xVelocity <= DisplayManager.getProperties().getWidth()-xSize)
		{
			x += xVelocity;
			if(xVelocity > 0)
			{
				xVelocity--;
			}
			else if(xVelocity < 0)
			{
				xVelocity++;
			}
		}
		else
		{
			x = DisplayManager.getProperties().getWidth()-xSize;
			xVelocity = 0;
		}

		//left
		if(x + xVelocity > 0)
		{
			x += xVelocity;
			if(xVelocity > 0)
			{
				xVelocity--;
			}
			else if(xVelocity < 0)
			{
				xVelocity++;
			}
		}
		else
		{
			x = 0;
			xVelocity = 0;
		}

		//bottom
		if(y + yVelocity <= DisplayManager.getProperties().getHeight()-ySize)
		{
			y += yVelocity;
		}
		else
		{
			y = DisplayManager.getProperties().getHeight()-ySize;
			yVelocity = 0;
		}

		//top
		if(y + yVelocity > 0)
		{
			y += yVelocity;
		}
		else
		{
			y = 0;
			yVelocity = 0;
		}
}
	
	public void draw()
	{
		DisplayManager.drawImage(texture, (int)x, (int)y);
	}
}
