package com.trentv4.game.logging;

/** Defines the level of the specified log message. */
public enum LogLevel
{
	/** An error message. Highly recommended to be visible. */
	ERROR,
	/** An important message. Recommended. */
	IMPORTANT, 
	/** A warning message. */
	WARNING,
	/** Not recommended, use for debugging only. */
	NOTE,
	/** Used by the engine for initialization output, use for initializing systems. */
	INIT_NOTE;
}
