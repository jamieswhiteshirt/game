package com.trentv4.game.logging;

import static com.trentv4.game.logging.LogLevel.ERROR;
import static com.trentv4.game.logging.LogLevel.IMPORTANT;
import static com.trentv4.game.logging.LogLevel.INIT_NOTE;
import static com.trentv4.game.logging.LogLevel.NOTE;
import static com.trentv4.game.logging.LogLevel.WARNING;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import com.trentv4.game.core.MainGame;

/**
 * Principal logging class. Contains only two methods:
 * <ul>
 * <li><i>initialize(LogLevel)</i>, which initializes the system. Calling this multiple times will
 * 									erase all previous log messages.
 * <li><i>log(LogLevel, Object)</i>, which will print to console "<i>[STRING] Object</i>".</li>
 * <li><i>log(Object)</i>, which will do the former but always prefaced with [ALERT].</li>
 * <li><i>saveLog(String)</i>, which will save all log messages to disk.</li>
 * </ul>
 */
public final class Logger
{
	private static ArrayList<String> logMessages;
	private static ArrayList<LogLevel> logLevels;
	private static LogLevel logLevel;

	/** Private constructor so this class cannot be instantiated. */
	private Logger() {}; 
	
	/** Initializes the logging system at <i>loggingLevel</i>, where any log messages short of that
	 * level will be saved to disk but not shown in console. */
	public static final void initialize(LogLevel loggingLevel)
	{
		logMessages = new ArrayList<String>();
		logLevels = new ArrayList<LogLevel>();
		logLevel = loggingLevel;
		log(INIT_NOTE, "Logging system initialized");
	}
	
	/** Prints a log message with a where <i>message</i> will be designated <i>Important</i> level.*/
	public static final void log(Object message)
	{
		String messagec = message.toString();
		logMessages.add(messagec);
		logLevels.add(LogLevel.IMPORTANT);
		if(isLoggable(LogLevel.IMPORTANT))
		{
			System.out.println("<IMPORTANT> " + message);
		}
	}
	
	/** Prints a log message with a custom <i>level</i> that <i>message</i> will be prefaced with.
	 * <br><br>
	 * For example, <i>log(LogLevel.NOTE, "Never gonna give you up")</i> will print 
	 * <ul>"<i>[NOTE] Never gonna give you up</i>"</ul> to console.*/
	public static final void log(LogLevel loggingLevel, String message)
	{
		logMessages.add(message);
		logLevels.add(loggingLevel);
		switch(loggingLevel)
		{
		case ERROR: if(isLoggable(loggingLevel)) {System.out.println("<ERROR> " + message);}
			break;
		case IMPORTANT: if(isLoggable(loggingLevel)) {System.out.println("<IMPORTANT> " + message);}
			break;
		case WARNING: if(isLoggable(loggingLevel)) {System.out.println("<WARNING> " + message);}
			break;
		case NOTE: if(isLoggable(loggingLevel)) {System.out.println("<NOTE> " + message);}
			break;
		case INIT_NOTE: if(isLoggable(loggingLevel)) {System.out.println("<INIT> " + message);}
			break;
		default:
			break;
		}
	}

	/** Writes a new file, saving all previous log messages to disk. */
	public static final void saveLog(String path)
	{
		try
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MainGame.getPath() + path)));
			for(int i = 0; i < logMessages.size(); i++)
			{
				writer.write(logMessages.get(i) + "\n");
			}
			writer.close();
		}
		catch(Exception e)
		{
			MainGame.crash(e);
		}
	}
	
	private static final boolean isLoggable(LogLevel loggingLevel)
	{
		if(logLevel == ERROR)
		{
			if(loggingLevel == ERROR)
			{
				return true;
			}
		}
		else if(logLevel == IMPORTANT)
		{
			if(loggingLevel == ERROR | loggingLevel == IMPORTANT)
			{
				return true;
			}
		}
		else if(logLevel == WARNING)
		{
			if(loggingLevel == ERROR | loggingLevel == IMPORTANT | loggingLevel == WARNING)
			{
				return true;
			}
		}
		else if(logLevel == NOTE)
		{
			if(loggingLevel == ERROR | loggingLevel == IMPORTANT | loggingLevel == WARNING | loggingLevel == NOTE)
			{
				return true;
			}
		}
		else if(logLevel == INIT_NOTE)
		{
			return true;
		}
		return false;
	}
}
