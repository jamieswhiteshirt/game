package com.trentv4.game.settings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import com.trentv4.game.core.MainGame;
import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;

/**
 * Main class that is used to track program information. This is mainly used by the DisplayManager. This
 * class handles all fields as private and provides the following accessors:
 * <ul>
 * <li><i>getWidth()</i></li>
 * <li><i>getHeight()</i></li>
 * <li><i>getMaxFPS()</i></li>
 * <li><i>isShowingFPS()</i></li>
 * <li><i>isShowingAdvancedFPS()</i></li>
 * </ul>
 * In addition, this class provides the method <i>saveProperties(Properties, String)</i>, which will 
 * encode and save the <i>Properties</i> object to disk at the <i>String</i> location.
 */
public class Properties
{
	private int WIDTH;
	private int HEIGHT;
	private int MAX_FPS;
	private boolean SHOW_FPS;
	private boolean SHOW_ADVANCED_FPS;
	
	/** Creates a new, default Properties object. This is auto-populated with generic information. */
	public Properties()
	{
		this.WIDTH = 400;
		this.HEIGHT = 400;
		this.MAX_FPS = 1000;
		this.SHOW_FPS = true;
		this.SHOW_ADVANCED_FPS = true;
	}
	
	/** Creates a new Properties object based on the file located at <i>path</i>. It is not required
	 * to specify the entire path, only the local directory path, e.g. "<i>settings.txt</i>". 
	 * NOTE: Not Yet Implemented.*/
	public Properties(String path)
	{
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(new File(MainGame.getPath() + path)));
			String[] list = new String[5];
			for(int i = 0; i < list.length; i++)
			{
				list[i] = reader.readLine();
			}
			reader.close();
			
			this.WIDTH = Integer.parseInt(list[0].substring(6));
			this.HEIGHT = Integer.parseInt(list[1].substring(7));
			this.MAX_FPS = Integer.parseInt(list[2].substring(7));
			this.SHOW_FPS = Boolean.parseBoolean(list[3].substring(8));
			this.SHOW_ADVANCED_FPS = Boolean.parseBoolean(list[4].substring(16));
			Logger.log(SHOW_ADVANCED_FPS);
		}
		catch (Exception e)
		{
			Logger.log(LogLevel.ERROR, "Unable to load proper file!");
			this.WIDTH = 400;
			this.HEIGHT = 400;
			this.MAX_FPS = 1000;
			this.SHOW_FPS = true;
		}
	}
	
	/** Saves the provided Properties object to the provided <i>path</i>. It is not required
	 * to specify the entire path, only the local directory path, e.g. "<i>settings.txt</i>". 
	 * NOTE: Not Yet Implemented.*/
	public static final void saveProperties(Properties properties, String path)
	{
		try
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(MainGame.getPath() + path)));
			writer.write("width=" + properties.WIDTH + "\n");
			writer.write("height=" + properties.HEIGHT + "\n");
			writer.write("maxfps=" + properties.MAX_FPS + "\n");
			writer.write("showfps=" + properties.SHOW_FPS + "\n");
			writer.write("showadvancedfps=" + properties.SHOW_ADVANCED_FPS + "\n");
			writer.close();
		}
		catch(Exception e)
		{
			Logger.log(LogLevel.ERROR, "Unable to save proper file!");
			MainGame.crash(e);
		}
	}

	/** Returns the width of the window. */
	public int getWidth()
	{
		return WIDTH;
	}
	
	/** Returns the height of the window. */
	public int getHeight()
	{
		return HEIGHT;
	}
	
	/** Returns the max FPS that the display manager will draw. */
	public int getMaxFPS()
	{
		return MAX_FPS;
	}
	
	/** Returns if FPS will be shown or not. */
	public boolean isShowingFPS()
	{
		return SHOW_FPS | SHOW_ADVANCED_FPS;
	}

	/** Returns if advanced FPS will be shown or not. This will show both regular FPS and game loop. */
	public boolean isShowingAdvancedFPS()
	{
		return SHOW_ADVANCED_FPS;
	}
}
