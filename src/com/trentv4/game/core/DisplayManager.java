package com.trentv4.game.core;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;
import com.trentv4.game.settings.Properties;

/**
 * Principal class that handles the display. This creates a Swing window and uses AWT Graphics to draw
 * an internal BufferedImage frame before rendering it. This window will call <i>MainGame.kill()</i>
 * on exit. This class provides several methods for manipulating the frame:
 * <ul>
 * <li>BufferedImage <i>getImage()</i>, which returns the frame that is waiting to be rendered,</li>
 * <li>void <i>drawImage(String, int x, int y)</i>, which will load and draw an image at 
 * 											coordinate x, y on the next frame.</li>
 * <li>JFrame <i>getFrame()</i>, which returns the JFrame that is rendering the image.</li>
 * <li>Properties <i>getProperties()</i>, which returns the current Properties object.</li>
 * <li>boolean <i>isInitialized()</i>, which returns if the manager is initialized yet.</li>
 * </ul>
 * In addition, this class has an <i>init</i> method which is called from <i>MainGame</i>. This method
 * is NOT safe to call, and will generally fuck up everything if you do. Don't!
 */
public class DisplayManager extends JPanel
{
	private static final long serialVersionUID = 1L;
	private static Properties properties;
	private static BufferedImage nextFrame;
	private static BufferedImage currentFrame;
	private static JFrame frame;
	private static int lastSecondFPS;
	private static boolean isInitialized = false;
	
	/** Entry point of the program. Creates the window based on the provided <i>newProperties</i>. 
	 * This method is NOT safe to call! If you do, generally, it'll fuck everything up.*/
	public static final void init(Properties newProperties)
	{
		Logger.log(LogLevel.INIT_NOTE, "Initializing display manager...");
		properties = newProperties;
		nextFrame = new BufferedImage(properties.getWidth(), properties.getHeight(), BufferedImage.TYPE_INT_ARGB);
		frame = new JFrame();
		frame.getContentPane().add(new DisplayManager());
		frame.setDefaultCloseOperation(3); //will exit when 'x'ed out
		frame.setVisible(true);
		frame.setBounds(0, 0, properties.getWidth(), properties.getWidth());
		frame.addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(final WindowEvent winEvt) 
            {
            	MainGame.kill();
            }
        });
		frame.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				InputMapper.setDown(e.getKeyCode());
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				InputMapper.setUp(e.getKeyCode());
			}
		});
		Logger.log(LogLevel.INIT_NOTE, "Display Manager initialized!");
		isInitialized = true;
		
		//Begin display.
		
		long timeStart = System.nanoTime();
		int tick = 0;
		int tickNanoTime = (int)(1000000000 / properties.getMaxFPS());//max framerate is 1,000
		long epoch = System.currentTimeMillis();
		int ticksPerSecond = 0;
		while(MainGame.isAlive())
		{
			long relativeTime = System.nanoTime() - timeStart;
			int newTick = (int)(relativeTime / tickNanoTime);
			int ticksToProcess = newTick - tick;

			tick = newTick;

			if(ticksToProcess >= 1)
			{
				frame.repaint();
				ticksPerSecond++;
			}
			if(System.currentTimeMillis() - epoch > 1000)
			{
				epoch = System.currentTimeMillis();
				lastSecondFPS = ticksPerSecond;
				ticksPerSecond = 0;
			}
		}
	}
	
	//Utility methods
	
	/** Returns the current JFrame. */
	public JFrame getJFrame()
	{
		return frame;
	}

    /** Checks if the Display Manager is initialized yet. */
	public static boolean isInitialized()
	{
		return isInitialized;
	}
	
	private static boolean canDraw()
	{
		return nextFrame != null;
	}
	
	/** Returns the current Properties object. */
	public static Properties getProperties()
	{
		return properties;
	}

	/** Gets the FPS from the last second. */
	public static int getFPS()
	{
		return lastSecondFPS;
	}
	
	//Non-fun required bits to make AWT behave properly.
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(currentFrame, 0, 0, null);
		if(properties.isShowingFPS())
		{
			g.drawString("FPS: " + getFPS(), 0, 10);
		}
		if(properties.isShowingAdvancedFPS())
		{
			g.drawString("Game loop: " + MainGame.getGameLoopFPS(), 0, 20);
		}

	}
	
    @Override
    public Dimension getPreferredSize() 
    {
        return new Dimension(properties.getWidth(), properties.getHeight());
    }
    
    @Override
    public Dimension getMinimumSize() 
    {
    	return this.getPreferredSize();
    }

    //Now on to the fun parts:
    
    /** Returns the current frame. Be aware that this frame is the next frame to be rendered, not the
     * frame that is currently being shown. */
    public static BufferedImage getImage()
	{
		return currentFrame;
	}

    /** Clears the image so the next frame may be drawn cleanly. */
    public static final void clear()
    {
    	if(canDraw())
    	{
    		currentFrame = nextFrame;
    		nextFrame = new BufferedImage(properties.getWidth(), properties.getHeight(), BufferedImage.TYPE_INT_ARGB);
    	}
    }
    
    /** Loads and draws a provided <i>image</i> on the current frame. */
    public static void drawImage(String image, int x, int y)
    {
    	if(canDraw())
    	{
        	BufferedImage i = ImageHandler.load(image);
            Graphics g = nextFrame.getGraphics();
            g.drawImage(i, x, y, null);
    	}
    }
}
