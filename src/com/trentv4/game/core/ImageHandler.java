package com.trentv4.game.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;

/** Main image-loading class. This class only contains one method, <i>load(String)</i>, which will
 * return a BufferedImage for usage.*/
public class ImageHandler
{
	private static HashMap<String, BufferedImage> textureMap;
	
	public static final void init()
	{
		textureMap = new HashMap<String, BufferedImage>();
	}
	
	/** Loads and returns a custom BufferedImage from <i>path</i>. This is checked against a HashMap
	 * containing all previously-loaded images to prevent unnecessary disk usage.*/
	public static BufferedImage load(String path)
	{
		try
		{
			if(textureMap.containsKey(path))
			{
				return textureMap.get(path);
			}
			BufferedImage i = ImageIO.read(new File(MainGame.getPath() + path));
			textureMap.put(path, i);
			return i;
		}
		catch(Exception e)
		{
			MainGame.crash(e);
			return new BufferedImage(0, 0, BufferedImage.TYPE_INT_ARGB);
		}
	}
}
