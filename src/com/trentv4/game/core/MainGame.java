package com.trentv4.game.core;

import java.io.File;

import com.trentv4.game.logging.LogLevel;
import com.trentv4.game.logging.Logger;
import com.trentv4.game.settings.Properties;

/**
 * Main class of the program. This handles the life status of the program, all tracked threads,
 * stores the program directory, and provides several useful methods in 
 * the the general execution of the program. These methods include:
 * <ul>
 * <li>void <i>kill()</i>, which is used to universally end the program,</li>
 * <li>void <i>crash(Exception)</i>, which is used to print an error log and then kill the program,</li>
 * <li>boolean <i>isAlive()</i>, which is used to check if the program is still alive,</li>
 * <li>String <i>getPath()</i>, which is used to retreive the current folder path.</li>
 * </ul> 
 * This class also contains a private Thread SystemLoop, which runs <i>GameLoop.run()</i> at maximum speed.
 */
public class MainGame
{
	private static boolean isRunning = false;
	private static MainGame theGame;
	private static String path;
	public static String version = "0.1 Pre-Alpha";
	private static int lastSecondFPS = 0;
	
	/** Main entry point of the program. Accepts no arguments. */
	public static void main(String[] args) //will never be thrown
	{
		Logger.initialize(LogLevel.INIT_NOTE);
		try
		{
			InputMapper.initialize();
			theGame = new MainGame();
			setPath(new File("").getCanonicalPath() + "/"); //ugly hack but it works ¯\_(ツ)_/¯ -- NO IT DOESN'T
			Logger.log(LogLevel.INIT_NOTE, "Created path at " + path);
			ImageHandler.init();
			Logger.log(LogLevel.INIT_NOTE, "Initialized ImageHandler");
			isRunning = true;
			Logger.log(LogLevel.INIT_NOTE, "Game is now running!");
			Thread t = new Thread(theGame.new SystemLoop());
			t.start();
			Logger.log(LogLevel.INIT_NOTE, "SystemLoop thread started.");
			DisplayManager.init(new Properties("settings.txt"));
		}
		catch(Exception e)
		{
			crash(e);
		}
	}
	
	/** Kills the program. */
	public static void kill()
	{
		Logger.log("Program is being killed.");
		isRunning = false;
		Properties.saveProperties(DisplayManager.getProperties(), "settings.txt");
		Logger.saveLog("log.txt");
	}
	
	/** Prints the Exception and then kills the program. */
	public static final void crash(Exception e)
	{
		Logger.log("Program has crashed! Printing error log: ");
		e.printStackTrace();
		Logger.log(LogLevel.ERROR, e.getMessage());
		kill(); //RIP
	}

	/** Checks the status of the program (if executing or not). This allows for safe, 
	 * <i>ConcurrentModificationException</i>-free heartbeat checking. */
	public static boolean isAlive()
	{
		return isRunning;
	}

	/** Gets the FPS from the last second for the game loop. */
	public static int getGameLoopFPS()
	{
		return lastSecondFPS;
	}

	/** Returns the current directory, ready-formatted for use,
	 *  e.g. "<i>/home/trent/execution_folder/</i>". */
	public static String getPath()
	{
		return path;
	}

	private static void setPath(String path)
	{
		MainGame.path = path;
	}

	private class SystemLoop implements Runnable
	{
		@Override
		public void run()
		{
			long timeStart = System.nanoTime();
			int tick = 0;
			int tickNanoTime = (int)(1000000000 / 500);//max game tick per second is 1,000
			long epoch = System.currentTimeMillis();
			int ticksPerSecond = 0;
			while(isAlive())
			{
				long relativeTime = System.nanoTime() - timeStart;
				int newTick = (int)(relativeTime / tickNanoTime);
				int ticksToProcess = newTick - tick;

				tick = newTick;

				if(ticksToProcess >= 1)
				{
					GameLoop.run();
					ticksPerSecond++;
				}
				if(System.currentTimeMillis() - epoch > 1000)
				{
					epoch = System.currentTimeMillis();
					lastSecondFPS = ticksPerSecond;
					ticksPerSecond = 0;
				}
			}
		}
	}
}
