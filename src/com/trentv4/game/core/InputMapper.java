package com.trentv4.game.core;

import static java.awt.event.KeyEvent.VK_A;
import static java.awt.event.KeyEvent.VK_D;
import static java.awt.event.KeyEvent.VK_SPACE;

public class InputMapper
{
	private static boolean[] buttons;
	//{A:0, D:1, SPACE:2}
	
	public static final void initialize()
	{
		buttons = new boolean[3];
		for(int i = 0; i < buttons.length; i++)
		{
			buttons[i] = false;
		}
	}
	
	public static final void tick()
	{
		if(buttons[0]) //A
		{
			GameLoop.player.xVelocity--;
		}
		if(buttons[1]) //D
		{
			GameLoop.player.xVelocity++;
		}
		if(buttons[2]) //Space
		{
			if(GameLoop.player.yVelocity == 0)
			{
				GameLoop.player.yVelocity -= 1.0;
			}
		}
	}
	
	public static final void setDown(int key)
	{
		if(key == VK_A)
		{
			buttons[0] = true;
		}
		if(key == VK_D)
		{
			buttons[1] = true;
		}
		if(key == VK_SPACE)
		{
			buttons[2] = true;
		}
	}

	public static final void setUp(int key)
	{
		if(key == VK_A)
		{
			buttons[0] = false;
		}
		if(key == VK_D)
		{
			buttons[1] = false;
		}
		if(key == VK_SPACE)
		{
			buttons[2] = false;
		}
	}
}
