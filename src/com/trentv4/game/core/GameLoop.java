package com.trentv4.game.core;

import com.trentv4.game.content.mob.Entity;

/** Primary loop class for the program. This class only exposes one method: <i>run()</i>, which
 * will run differing methods depending on program state. */
public final class GameLoop
{
	public static Entity player = new Entity("dude.png", 50, 50, 20, 30);
	
	private GameLoop(){}
	
	/** Executes one iteration of the game loop. */
	public static final void run()
	{
		if(DisplayManager.isInitialized())
		{
			InputMapper.tick();
			runIngame();
		}
	}
	
	private static final void runIngame()
	{
		DisplayManager.clear();
		DisplayManager.drawImage("castle.png", 0, 0);
		player.applyPhysics();
		player.draw();
	}
}
