# Game #

This is a random game created by Trent VanSlyke. It's origin is midnight boredom and it's nature is unforeseen.

### How do I get going? ###

* Git clone.
* Add project to Eclipse (.gitignore is only set up for Eclipse).
* Run. It uses native Swing for windows and AWT for graphics.

### Who do I talk to? ###

* Trent VanSlyke. Found here: trentvanslyke@gmail.com, @trentv4, jamieswhiteshirt.com/trentv4
